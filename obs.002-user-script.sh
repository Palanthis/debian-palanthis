#!/bin/bash
set -e
#
##########################################################
# Author 	: 	Palanthis (palanthis@gmail.com)
# Website 	: 	http://github.com/Palanthis
# License	:	Distributed under the terms of GNU GPL v3
# Warning	:	These scripts come with NO WARRANTY!!!!!!
##########################################################

# Temporary fix to Adapta Theme
sudo tar xzf tarballs/adapta.tar.gz -C /usr/share/themes/ --overwrite

# Copy over some of my favorite fonts, themes and icons
sudo [ -d /usr/share/fonts/OTF ] || sudo mkdir /usr/share/fonts/OTF
sudo [ -d /usr/share/fonts/TTF ] || sudo mkdir /usr/share/fonts/TTF
sudo [ -d /usr/share/icons/Arch ] || sudo mkdir /usr/share/icons/Arch
sudo tar xzf tarballs/fonts-otf.tar.gz -C /usr/share/fonts/OTF/ --overwrite
sudo tar xzf tarballs/fonts-ttf.tar.gz -C /usr/share/fonts/TTF/ --overwrite
sudo tar xzf tarballs/buuf-icons.tar.gz -C /usr/share/icons/ --overwrite

# Write out new configuration files
tar xzf tarballs/config.tar.gz -C ~/ --overwrite
tar xzf tarballs/local.tar.gz -C ~/ --overwrite

# Copy over Menulibre items
tar xzf tarballs/applications.tar.gz -C ~/.local/share/ --overwrite
tar xzf tarballs/xfce-applications-menu.tar.gz -C ~/.config/menus/ --overwrite

# Install wallpapers
[ -d ~/Wallpapers ] || mkdir ~/Wallpapers
[ -d ~/StarWarsWallpapers ] || mkdir ~/StarWarsWallpapers
tar xzf tarballs/wallpapers1.tar.gz -C ~/Wallpapers/
tar xzf tarballs/wallpapers2.tar.gz -C ~/Wallpapers/
tar xzf tarballs/starwarswallpapers.tar.gz -C ~/StarWarsWallpapers/

# Copy Conky
[ -d ~/.conky ] || mkdir ~/.conky
tar xzf tarballs/conky.tar.gz -C ~/.conky/

# Add screenfetch to .bashrc
# echo "screenfetch" >> ~/.bashrc

# Replace .bashrc (for use with Tilix)
sudo tar xzf tarballs/bashrc.tar.gz -C ~/ --overwrite

# Add lines to .bashrc for ccache
echo 'export PATH="/usr/lib/ccache/bin/:$PATH"' >> ~/.bashrc
echo 'export MAKEFLAGS="-j9 -l8"' >> ~/.bashrc

echo " "
echo "All done!"
