#!/bin/bash
set -e
#
##########################################################
# Author 	: 	Palanthis (palanthis@gmail.com)
# Website 	: 	http://github.com/Palanthis
# License	:	Distributed under the terms of GNU GPL v3
# Warning	:	These scripts come with NO WARRANTY!!!!!!
##########################################################

# Add contrib and non-free to APT
sed -i 's/main/main\ contrib\ non-free/' /etc/apt/sources.list

# Install Build Essentials
apt install build-essential module-assistant

# Prep
m-a prepare

echo "You may now run VBoxAdditions for Linux."
echo "All done!"